use clap::Parser;
use memchr::memmem;
use pcap::{Capture, Device, Packet};
use virt::{connect::Connect, domain::Domain, sys::VIR_DOMAIN_SHUTOFF};

const BFP_FILTER_PROGRAM: &str =
    "ether proto 0x0842 or udp port 0 or udp port 7 or udp port 9 or udp port 40000";

const WOL_SIGNATURE_LEN: usize = 6;
const WOL_SIGNATURE: [u8; WOL_SIGNATURE_LEN] = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff];

const WOL_MAC_REPEATS: usize = 16;
const MAC_LEN: usize = 6;

const WOL_PAYLOAD_SIZE: usize = WOL_SIGNATURE_LEN + WOL_MAC_REPEATS * MAC_LEN;

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Args {
    /// The name of the device to listen for WOL packets
    #[arg(short, long)]
    device: String,

    /// The name of the virtual machine to wake, defaults to any VM with a matching MAC address
    #[arg(short, long)]
    virtual_machine: Option<String>,
}

#[derive(Debug, PartialEq)]
struct MacAddr<'packet> {
    addr: &'packet [u8],
}

#[derive(Debug)]
struct WolPayload<'packet> {
    wol_signature: &'packet [u8],
    mac_addrs: [MacAddr<'packet>; WOL_MAC_REPEATS],
}

impl<'packet> WolPayload<'packet> {
    fn new(data: &'packet [u8]) -> WolPayload {
        let mut i = 0;
        let mut mac_addrs: Vec<MacAddr<'packet>> = Vec::new();
        while i < WOL_MAC_REPEATS {
            let start = WOL_SIGNATURE_LEN + i * MAC_LEN;
            mac_addrs.push(MacAddr {
                addr: &data[start..start + MAC_LEN],
            });
            i += 1;
        }

        return WolPayload {
            wol_signature: &data[0..WOL_SIGNATURE_LEN],
            mac_addrs: mac_addrs.try_into().unwrap(),
        };
    }

    fn is_valid(&self) -> bool {
        if self.wol_signature != WOL_SIGNATURE {
            return false;
        };

        return self.mac_addrs.iter().all(|x| *x == self.mac_addrs[0]);
    }
}

fn main() {
    let args = Args::parse();

    let device = Device::list()
        .unwrap()
        .into_iter()
        .find(|x| x.name == args.device);

    if let Some(device) = device {
        let mut cap = Capture::from_device(device)
            .unwrap()
            // .promisc(true)
            .immediate_mode(true)
            .open()
            .unwrap();

        let _ = cap
            .filter(BFP_FILTER_PROGRAM, true)
            .expect("Unable to set pcap filter program");

        while let Ok(packet) = cap.next_packet() {
            if let Some(wol) = get_wol_payload(&packet) {
                if let Ok(qemu_connection) = Connect::open("qemu:///system") {
                    if let Some(domain) =
                        domain_from_mac(&qemu_connection, &wol, &args.virtual_machine)
                    {
                        println!("{:?}", domain);
                        let (vm_state, _) = domain
                            .get_state()
                            .expect("Unable to get current state of match virtual machine.");
                        println!("State: {}", vm_state);
                        if vm_state == VIR_DOMAIN_SHUTOFF {
                            if let Ok(_start) = domain.create() {
                                println!("Started: {}", domain.get_id().unwrap());
                            }
                        }
                    }
                }
            }
        }
    }
}

fn get_wol_payload<'packet>(packet: &'packet Packet<'packet>) -> Option<WolPayload<'packet>> {
    let mut search_iter = memmem::find_iter(packet.data, &WOL_SIGNATURE);
    while let Some(possible_wol_payload_location) = search_iter.next() {
        let wol = WolPayload::new(
            &packet.data
                [possible_wol_payload_location..possible_wol_payload_location + WOL_PAYLOAD_SIZE],
        );
        if wol.is_valid() {
            return Some(wol);
        }
    }

    return None;
}

fn domain_from_mac(
    qemu_connection: &Connect,
    wol: &WolPayload,
    vm_name: &Option<String>,
) -> Option<Domain> {
    let mut domains: Vec<Domain> = Vec::new();
    if let Some(vm_name) = vm_name {
        if let Ok(domain) = Domain::lookup_by_name(qemu_connection, vm_name.as_str()) {
            domains.push(domain);
        }
    } else {
        domains = qemu_connection.list_all_domains(0).unwrap();
    }

    println!("domains: {:?}", domains);
    // println!("{:?}", format!(
    //     "<mac address='{:02x?}:{:02x?}:{:02x?}:{:02x?}:{:02x?}:{:02x?}'/>",
    //     wol.mac_addrs[0].addr[0],
    //     wol.mac_addrs[0].addr[1],
    //     wol.mac_addrs[0].addr[2],
    //     wol.mac_addrs[0].addr[3],
    //     wol.mac_addrs[0].addr[4],
    //     wol.mac_addrs[0].addr[5]
    // ).as_str());

    for domain in domains.iter() {
        let xml = domain
            .get_xml_desc(0)
            .expect("Unable to extract XML for domain.");
        // println!("XML: {}", xml);
        println!("Contains: {:?}", xml.contains(
            format!(
                "<mac address='{:02x?}:{:02x?}:{:02x?}:{:02x?}:{:02x?}:{:02x?}'/>",
                wol.mac_addrs[0].addr[0],
                wol.mac_addrs[0].addr[1],
                wol.mac_addrs[0].addr[2],
                wol.mac_addrs[0].addr[3],
                wol.mac_addrs[0].addr[4],
                wol.mac_addrs[0].addr[5],
            )
            .as_str(),
        ));
        if xml.contains(
            format!(
                "<mac address='{:02x?}:{:02x?}:{:02x?}:{:02x?}:{:02x?}:{:02x?}'/>",
                wol.mac_addrs[0].addr[0],
                wol.mac_addrs[0].addr[1],
                wol.mac_addrs[0].addr[2],
                wol.mac_addrs[0].addr[3],
                wol.mac_addrs[0].addr[4],
                wol.mac_addrs[0].addr[5],
            )
            .as_str(),
        ) {
            println!("Matched.");
            return Some(Domain::lookup_by_name(qemu_connection, domain.get_name().unwrap().as_str()).unwrap());
        }
    }
    println!("No match?");
    return None;
}
